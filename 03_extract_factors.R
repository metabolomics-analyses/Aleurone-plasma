# Libraries ---------------------------------------------------------------
library(magrittr)
library(dplyr)
library(readxl)


# Helper functions --------------------------------------------------------
as.numeric_silent <- function(x) suppressWarnings(as.numeric(x))


# Get factors -------------------------------------------------------------
factors <- matrix(nrow = length(settings$general$files),ncol = 5) %>% as.data.frame %>% setNames(c("Person","Time","Meal","Treatment","Runorder"))

# Person
factors$Person <- settings$general$files %>% basename %>% substr(24,25) %>% as.numeric_silent %>% ifelse(.==12 & !is.na(.),11,.) %>% as.factor # there is no person 12. It is mislabelled

# time
factors$Time   <- settings$general$files %>% basename %>% substr(23,23) %>% as.numeric_silent %>% ifelse(.>2,NA,.) %>% factor(labels=c("Before","After"))

# meal
factors$Meal <- ifelse(grepl("_W_",        settings$general$files,fixed=TRUE),"W",    factors$Meal)
factors$Meal <- ifelse(grepl("_E_",        settings$general$files,fixed=TRUE),"E",    factors$Meal)
factors$Meal <- factors$Meal %>% as.factor

# treatment
factors %<>% 
            mutate(Treatment = ifelse(Time=="Before","Baseline", Treatment)) %>% 
            mutate(Treatment = ifelse(Time=="After" & Meal=="W","Placebo", Treatment)) %>% 
            mutate(Treatment = ifelse(Time=="After" & Meal=="E","Aleurone", Treatment)) %>% 
            mutate(Treatment = as.factor(Treatment))


# runorder
factors$Runorder <- settings$general$files %>% basename %>% substr(2,4) %>% as.integer



# plate
read_excel("sequence.xlsx") %>% 
                                mutate(Runorder = substr(Runorder,2,4),Runorder = as.integer(Runorder)) %>% 
                                right_join(factors,by="Runorder") %>% 
                                mutate(Plate = as.factor(Plate))  ->
factors


# Add QCs and blanks to treatment
factors %<>%  mutate(Treatment = as.character(Treatment)) %>% 
              mutate(Treatment = ifelse(grepl("_Solvent_",  settings$general$files,ignore.case=TRUE),"Solvent", Treatment)) %>%
              mutate(Treatment = ifelse(grepl("_ratioSTD_", settings$general$files,fixed=TRUE),"ratioSTD",Treatment)) %>% 
              mutate(Treatment = ifelse(grepl("_Blank",     settings$general$files,fixed=TRUE),"Blank",   Treatment)) %>%
              mutate(Treatment = ifelse(grepl("_QC", settings$general$files,fixed=TRUE),"QC", Treatment)) %>%
              mutate(Treatment = ifelse(grepl("_Pool_QC", settings$general$files,fixed=TRUE),"Pool_QC", Treatment)) %>% 
              mutate(Treatment = as.factor(Treatment))
  
  


factors <- as.data.frame(factors)

